#!/bin/bash

#this program dump postgres databases
#use rsync to send databases dumped
 
NOW=$(date +"%m-%d-%Y--%H--%M--%S")
array_checkdb[0]=""
cpt=0
declare -A map    # required: declare explicit associative array

database_list() {
	#TODO : databases owned by this user
	#LIST=$(sudo -u postgres psql -l | awk '{ print $1}' | grep -vE '^-|^List|^Name|template[0|1]|postgres|\||\(|'^no_owned...)
	LIST=$(sudo -u postgres psql -l | awk '{ print $1}' | grep -vE '^-|^List|^Name|template[0|1]|postgres|\||\(')
	for db in $LIST
	do
		echo $db
		array_checkdb[$cpt]=$db
		cpt=$(($cpt+1))
	done

	
	for key in "${!array_checkdb[@]}"; do map[${array_checkdb[$key]}]="$key"; done 
}
 

dump_database() {
	DIR=/home/gillesmeyomessestephane/backup_db/psql
	[ ! -d $DIR ] && mkdir -p $DIR || :
	if [ $database_name == "ALL" ];then
		echo "ready to dump ALL databases please wait..."
		for db in $LIST
		do
			sudo -u postgres pg_dump $db | gzip -c >  $DIR/$NOW.$db.gz
		done
		echo "databases are dumped"
	else
		echo "ready to dump $database_dump please wait..."
		sudo -u postgres pg_dump $database_name | gzip -c >  $DIR/$NOW.$database_name.gz
		echo "database $database_dump is dumped"
	fi
}

rsync_database_dumped() {
	sudo rsync -e ssh -avz --delete-after /home/source $uUSER@$IP:/backupodoo/
}


#dump all databases
database_list
database_name="ALL"
dump_database
#rsync_database_dumped

